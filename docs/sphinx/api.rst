.. _sphinx_api:

#############
|project| API
#############

*********
|project|
*********

.. toctree::
    :maxdepth: 2

.. _micromorphic_linear_elasticity_source:

micromorphic_linear_elasticity.cpp
============

.. doxygenfile:: micromorphic_linear_elasticity.cpp

micromorphic_linear_elasticity.h
==========

.. doxygenfile:: micromorphic_linear_elasticity.h

****************
Abaqus interface
****************

micromorphic_linear_elasticity_umat.cpp
=================

.. doxygenfile:: micromorphic_linear_elasticity_umat.cpp

micromorphic_linear_elasticity_umat.h
===============

.. doxygenfile:: micromorphic_linear_elasticity_umat.h
